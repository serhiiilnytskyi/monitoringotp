export interface Task {
  id: string;
  SCID: string;
  reporter: string;
  initiator: string;
  summary: string;
  description: string;
  priority: boolean;
  employees: bigint;
  registrator: string;
  fromdate: Date;
  todate: Date;
  nextdate: Date;
  status: boolean;
  oneday: boolean;
  everyday: boolean;
  everyweekly: boolean;
  everymontly: boolean;
}
