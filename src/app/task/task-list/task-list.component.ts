import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import { Task } from '../../_interface/task.model';
import {RepositoryService} from '../../shared/repository.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit, AfterViewInit{

  public displayedColumns = ['baseTask', 'reporter', 'initiator', 'summary', 'description', 'priority', 'fromDate', 'toDate', 'status'];

  public dataSourse = new MatTableDataSource<Task>();

  @ViewChild(MatSort) sort: MatSort;

  constructor(private repoService: RepositoryService) { }

  ngOnInit() {
    this.getAllTask();
  }

  ngAfterViewInit(): void {
    this.dataSourse.sort = this.sort;
  }

  public getAllTask = () => {
    this.repoService.getData('api/tasks')
      .subscribe(res => {
        this.dataSourse.data = res as Task[];
      });
  }

  public redirectToDetails = (id: string) => {
  }

  public redirectToUpdate = (id: string) => {
  }

  public redirectToDelete = (id: string) => {
  }
}
