import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskListComponent} from './task-list/task-list.component';
import {TaskRoutingModule} from './task-routing/task-routing.module';
import {MaterialModule} from '../material/material.module';

@NgModule({
  declarations: [TaskListComponent],
  imports: [
    CommonModule,
    TaskRoutingModule,
    MaterialModule
  ]
})
export class TaskModule {
}
