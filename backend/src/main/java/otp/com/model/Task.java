package otp.com.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "dbo.task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "strOTSCID")
    private String SCID;

    @Column(name = "strOTReportingPersonLogin")
    private String reporter;

    @Column(name = "strOTInitiatorLogin")
    private String initiator;

    @Column(name = "strOTSummary")
    private String summary;

    @Column(name = "strOTDescription")
    private String description;

    @Column(name = "strOTPriority")
    private Boolean priority;

    @Column(name = "nOTIncedentEmployees")
    private Integer employeesCount;

    @Column(name = "strRegistratorInfo")
    private Boolean registratorInfo;

    private Date fromDate;

    private Date toDate;

    private Date nextDate;

    private Boolean status;

    private Boolean oneday;

    private Boolean everyday;

    private Boolean everyweekly;

    private Boolean everymonthly;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSCID() {
        return SCID;
    }

    public void setSCID(String SCID) {
        this.SCID = SCID;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPriority() {
        return priority;
    }

    public void setPriority(Boolean priority) {
        this.priority = priority;
    }

    public Integer getEmployeesCount() {
        return employeesCount;
    }

    public void setEmployeesCount(Integer employeesCount) {
        this.employeesCount = employeesCount;
    }

    public Boolean getRegistratorInfo() {
        return registratorInfo;
    }

    public void setRegistratorInfo(Boolean registratorInfo) {
        this.registratorInfo = registratorInfo;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getNextDate() {
        return nextDate;
    }

    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getOneday() {
        return oneday;
    }

    public void setOneday(Boolean oneday) {
        this.oneday = oneday;
    }

    public Boolean getEveryday() {
        return everyday;
    }

    public void setEveryday(Boolean everyday) {
        this.everyday = everyday;
    }

    public Boolean getEveryweekly() {
        return everyweekly;
    }

    public void setEveryweekly(Boolean everyweekly) {
        this.everyweekly = everyweekly;
    }

    public Boolean getEverymonthly() {
        return everymonthly;
    }

    public void setEverymonthly(Boolean everymonthly) {
        this.everymonthly = everymonthly;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", SCID='" + SCID + '\'' +
                ", reporter='" + reporter + '\'' +
                ", initiator='" + initiator + '\'' +
                ", summary='" + summary + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                ", employeesCount=" + employeesCount +
                ", registratorInfo=" + registratorInfo +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", nextDate=" + nextDate +
                ", status=" + status +
                ", oneday=" + oneday +
                ", everyday=" + everyday +
                ", everyweekly=" + everyweekly +
                ", everymonthly=" + everymonthly +
                '}';
    }
}
