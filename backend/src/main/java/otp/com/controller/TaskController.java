package otp.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import otp.com.model.Task;
import otp.com.repository.TaskRepository;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskController {
    @Autowired
    private TaskRepository taskRepository;

    @GetMapping(value = "/tasks", produces = "application/json")
    public List<Task> getAllTask() {
        return taskRepository.findAll();
    }

    @PostMapping(value = "/tasks")
    public void saveTask(@RequestBody Task task) {
        taskRepository.save(task);
    }
}
